'use strict';

const express      = require('express');
const bodyParser   = require('body-parser');
const app          = express();
const Web3	   = require('web3')
const AWS      = require('aws-sdk');
const fone     = require('./FoneHome.json')

var state = 0;
var owner_name = ['Ecobee', 'City Of Toronto'];
var device = ['Thermostat', 'Garbage'];
var type = ["Repair", "Pickuo"];


var job_id;
var a = [];
accounts();

AWS.config.update({region: 'us-east-1'});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
bodyParser.urlencoded({ extended: true })
  next();
});

// app.get('/accounts', (req, res) => {
//     //res.send("hello")
//     var web3 = new Web3("ws://18.209.231.241:8545");
//      return new Promise((resolve, reject) => {
//         web3.eth.getAccounts()
//         .then( (data) => 
//         {
//             web3.eth.getBalance(data[0]).then(balance => {
//                 res.send({"account":data[0], "balance":balance})
//                 resolve({"account":data[0], "balance":balance})
//                 web3.currentProvider.connection.close()
//             });
            
//         })
//         .catch(reject);
//     });

// });


app.get('/engineer', (req, res) => {

    var lambda = new AWS.Lambda();

    var params = {
      FunctionName: "elevate-lambda-iot",
      InvocationType: "RequestResponse"
     };
     lambda.invoke(params, function(err, data) {
       if (err){
        console.log(err, err.stack); // an error occurred
       } 
       else{
        console.log(data);    // successful response
        res.send(data["Payload"])
       }     
       /*
       data = {
        FunctionError: "", 
        LogResult: "", 
        Payload: <Binary String>, 
        StatusCode: 123
       }
       */
     });
    

});


app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());


app.post("/ballsdeep", function (req, res) {
    //console.log(req.body)
    if (req.body.depth === 1 && state === 0)
    {
        console.log("Broken")
        var web3 = new Web3("ws://18.209.231.241:8545");

        var myContract = new web3.eth.Contract(fone.abi, '0xfe3f8adf53cf44561d3e570745da3e309cca5844');
         return new Promise((resolve, reject) => {
           myContract.methods.createJob(req.body.type,req.body.owner_name,"George",req.body.device,Date.now().toString(),req.body.lat,req.body.lng).send({from: a[1], value: req.body.bounty, gas: 4000000})
            .then( (data) =>
            {
                //myContract.events.PostJob(function(error,event){
                  console.log(data.events.PostJob.returnValues._jobId);
                  job_id = data.events.PostJob.returnValues._jobId
                  resolve(data);
                  web3.currentProvider.connection.close();
                  state = 1;
                  res.send("suckeeeee");
                //});
                
            })
            .catch(reject);
        });         

    }
    else if (req.body.depth === 0 && state === 1)
    {
        console.log("Repaired");
        //console.log(a)

        //payout
        var web3 = new Web3("ws://18.209.231.241:8545");

        var myContract = new web3.eth.Contract(fone.abi, '0xfe3f8adf53cf44561d3e570745da3e309cca5844');
         return new Promise((resolve, reject) => {
           myContract.methods.completeJob(job_id).send({from: a[2], gas: 4000000})
            .then( (data) => 
            {
                console.log(data.events.ReleaseEscrow.returnValues);
                resolve(data);
                web3.currentProvider.connection.close();
                state = 0;
                res.send("no suckeeee");
            })
            .catch(reject);
        });          

    }  
    else
    {
        res.send("you dumb");
    } 

    
});


function accounts(){

    var web3 = new Web3("ws://18.209.231.241:8545");
     return new Promise((resolve, reject) => {
        web3.eth.getAccounts()
        .then( (data) => 
        {
                //console.log(data)
                resolve(data);
                a = data;
                web3.currentProvider.connection.close()
            
        })
        .catch(reject);
    }); 

}

app.listen(9000, () => {
        console.log('Server started on port ' + '9000');
});


